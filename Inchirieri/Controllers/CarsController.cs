﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inchirieri.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;

namespace Inchirieri.Controllers
{
    public class CarsController : Controller
    {
        public SqlConnection DbConnect()
        {
            string connectionString = @"Data Source=FIELD-AR\SQLEXPRESS;Initial Catalog=Inchirieri;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);

            return conn;
        }

        public SqlDataReader ExecuteQuery(string cmd, SqlConnection conn)
        {
            SqlCommand sqlCmd = new SqlCommand(cmd, conn);
            SqlDataReader reader = sqlCmd.ExecuteReader();

            return reader;
        }

        // GET: Cars
        [Route("cars")]
        
        public ActionResult Index()
        {
            List<Car> cars = new List<Car>();
            cars = CreateCars();
            ViewBag.cars = cars;

            return View();
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();
            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }

            Dictionary<int, string> fuel = GetFuelTypes();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> color = GetColors();

            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;
            ViewBag.Colors = color;

            return View(masina);
        }

        [HttpPost]
        public ActionResult Edit(int carID, string carModel, int regDate, int fuelType, int numSeats, int carColor, int gearBox, int rentPrice)
        {
            string sqlCmd = "Update Cars Set car_model=@model, car_fabricatedAt=@regDate, car_fuel=@fuel, car_seats=@seats, car_color=@color, car_transmission=@trs, car_rent_price=@price, updatedAt=SYSDATETIME() where car_id=@id";
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@regDate", regDate);
            cmd.Parameters.AddWithValue("@fuel", fuelType);
            cmd.Parameters.AddWithValue("@seats", numSeats);
            cmd.Parameters.AddWithValue("@color", carColor);
            cmd.Parameters.AddWithValue("@trs", gearBox);
            cmd.Parameters.AddWithValue("@price", rentPrice);
            cmd.Parameters.AddWithValue("@id", carID);

            cmd.ExecuteNonQuery();

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("cars/add")]
        public ActionResult Add()
        {
            Car masina = new Car();
            Dictionary<int, string>fuel = GetFuelTypes();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> colors = GetColors();

            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;
            ViewBag.Colors = colors;

            return View(masina);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("cars/add")]
        public ActionResult Add(string carModel, int regDate, int fuelType, int numSeats, int carColor, int gearBox, int rentPrice)
        {
            string sqlCmd = "INSERT INTO Cars (car_model, car_fuel, car_seats, car_color, car_transmission, car_rent_price, car_fabricatedAt, createdAt)" +
                            "VALUES (@model, @fuel, @seats, @color, @trs, @price, @regDate, SYSDATETIME());";

            SqlConnection conn = DbConnect();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@regDate", regDate);
            cmd.Parameters.AddWithValue("@fuel", fuelType);
            cmd.Parameters.AddWithValue("@seats", numSeats);
            cmd.Parameters.AddWithValue("@color", carColor);
            cmd.Parameters.AddWithValue("@trs", gearBox);
            cmd.Parameters.AddWithValue("@price", rentPrice);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("cars/rent/{carId}")]
        [Authorize(Roles="User")]
        public ActionResult Rent(int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();
            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }
            return View(masina);
        }

        [HttpPost]
        [Route("cars/rent/{carId}")]
        [Authorize(Roles = "User")]
        public ActionResult Rent(int carId, string startDate, string endDate, int totalPrice)
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            var userId = User.Identity.GetUserId();

            string sqlCmd = "Insert Into Rentals(order_carId, order_custId, order_startedAt, order_endedAt, order_totalPrice, createdAt) Values (@carId, @custId, @startDate, @endDate, @totalPrice, SYSDATETIME())";
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@carId", carId);
            cmd.Parameters.AddWithValue("@custId", userId);
            cmd.Parameters.AddWithValue("@startDate", startDate);
            cmd.Parameters.AddWithValue("@endDate", endDate);
            cmd.Parameters.AddWithValue("@totalPrice", totalPrice);

            cmd.ExecuteNonQuery();
                                                            
            return RedirectToAction("Rent");
        }
        public ActionResult Delete(int carId)
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            string sqlCmd = "Delete From Cars Where car_id=" + carId;
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("/");
        }

        [Route("cars/view/{carId}")]
        public ActionResult View(int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();

            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }
            ViewBag.image = "/Content/Images/car-" + carId + ".jpg";

            return View(masina);
        }

        public Dictionary<int, string> GetFuelTypes()
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            
            SqlDataReader reader = ExecuteQuery("Select * From FuelTypes", conn);

            Dictionary<int, string> fuel = new Dictionary<int, string>();
            while (reader.Read())
            {
                int fuelId = int.Parse(reader["fuel_id"].ToString());
                string fuelVal = reader["fuel_name"].ToString();

                fuel[fuelId] = fuelVal;
            }

            return fuel;
        }

        public Dictionary<int, string> GetTransmissions()
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlDataReader reader = ExecuteQuery("Select * From Transmissions", conn);

            Dictionary<int, string> trs = new Dictionary<int, string>();
            while (reader.Read())
            {
                int trId = int.Parse(reader["tr_id"].ToString());
                string trVal = reader["tr_name"].ToString();

                trs[trId] = trVal;
            }

            return trs;
        }
        public Dictionary<int, string> GetColors()
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlDataReader reader = ExecuteQuery("Select * From Colors", conn);

            Dictionary<int, string> color = new Dictionary<int, string>();
            while (reader.Read())
            {
                int colorId = int.Parse(reader["color_id"].ToString());
                string colorVal = reader["color_name"].ToString();

                color[colorId] = colorVal;
            }

            return color;
        }
        public List<Car> CreateCars()
        {
            List<Car> cars = new List<Car>();

            //Car c = new Car()
            //{
            // Id = 1,
            // Model = "C Klasse",
            // Fuel = "GPL",
            // Seats = 5,
            // Color = "PINK",
            // GearBox = "Automatic",
            // Price = 180
            //};

            SqlConnection conn = DbConnect();
            conn.Open();
            string sqlCmd = "Select Cars.*, Cars.car_model, FuelTypes.fuel_name, Transmissions.tr_name, Colors.color_name From Cars, FuelTypes, Transmissions,  Colors Where Cars.car_fuel = FuelTypes.fuel_id  AND Cars.car_transmission = Transmissions.tr_id AND Cars.car_color = Colors.color_id";

            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                Car masina = new Car();

                masina.Id = int.Parse(reader["car_id"].ToString());
                masina.Model = reader["car_model"].ToString();
                masina.RegDate = int.Parse(reader["car_fabricatedAt"].ToString());
                masina.Fuel = reader["fuel_name"].ToString();
                masina.Seats = int.Parse(reader["car_seats"].ToString());
                masina.Color = reader["color_name"].ToString();
                masina.GearBox = reader["tr_name"].ToString();
                masina.Price = int.Parse(reader["car_rent_price"].ToString());

                cars.Add(masina);
            }
            
            conn.Close();
            return cars;

        }
    }
}