﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Inchirieri.Startup))]
namespace Inchirieri
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
